import React, { useEffect, useState } from 'react';
import logo from './logo.svg';
import './App.css';

class Z {
  constructor(parentRef){
    this.parentRef = parentRef
    this.ref = parentRef === 0 ? parentRef : parentRef - 1
  }

  setRef = (newRef, parentRef) => {
    this.ref = newRef
    this.parentRef = parentRef || 0
  }
}

function App() {
  const [allZ, setAllZ] = useState([ new Z(0) ]);
  const [keyNow, setKeyNow] = useState('d')

  useEffect(() => {
    document.addEventListener('keydown', (e) => {
      console.log(e.key)
      return setKeyNow(e.key)
    })
    setInterval(() => {
      goTo()
    }, 1000)
  }, [])

  const createZ = () => {
    const prevZRef = allZ[allZ.length - 1].parentRef || 0
    setAllZ([ ...allZ, new Z(prevZRef)])
  }

  const goTo = () => {
    setAllZ(allZ.reduce((acc, el, index) => {
      console.log(keyNow)
      switch(el.ref) {
        case 4:
        case 9:
        case 14:
        case 19:
        case 24:
            const prevS = document.getElementById(el.ref)
            const nextS = document.getElementById(el.ref - 4)
            if (prevS) {
              prevS.style = "background-color: white;"
            }
            if (nextS) {
              nextS.style = "background-color: black;"
            }
          return [ ...acc, el.setRef(el.ref - 4)]
        default: 
          if (keyNow === 'd'){
            const prevD = document.getElementById(el.ref)
            const nextD = document.getElementById(el.ref + 1)
            if (prevD) {
              prevD.style = "background-color: white;"
            }
            if (nextD) {
              nextD.style = "background-color: black;"
            }
            return [ ...acc, el.setRef(el.ref + 1)]
          }
          if (keyNow === 's') {
            const prevD = document.getElementById(el.ref)
            const nextD = document.getElementById(el.ref + 4)
            if (prevD) {
              prevD.style = "background-color: white;"
            }
            if (nextD) {
              nextD.style = "background-color: black;"
            }
            return [ ...acc, el.setRef(el.ref + 4)]
          }
      }
    }, []))
  }

  return (
    <div className="App">
      <table>
        <tr><td id={0} /><td id={1} /><td id={2} /><td id={3} /><td id={4} /></tr>
        <tr><td id={5} /><td id={6} /><td id={7} /><td id={8} /><td id={9} /></tr>
        <tr><td id={10} /><td id={11} /><td id={12} /><td id={13} /><td id={14} /></tr>
        <tr><td id={15} /><td id={16} /><td id={17} /><td id={18} /><td id={19} /></tr>
        <tr><td id={20} /><td id={21} /><td id={22} /><td id={23} /><td id={24} /></tr>
      </table>
    </div>
  );
}

export default App;
